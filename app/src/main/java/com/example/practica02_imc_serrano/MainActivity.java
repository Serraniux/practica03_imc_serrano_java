package com.example.practica02_imc_serrano;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    private EditText Peso;
    private EditText Altura;
    private TextView Imc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void Calcular(View v){
        Peso=findViewById(R.id.lblPeso);
        Altura=findViewById(R.id.lblAltura);
        Imc=findViewById(R.id.lblIMC);
        if(Vali(Peso.getText().toString(),Altura.getText().toString())){
            inser(Conver(Peso.getText().toString()),Conver(Altura.getText().toString()));
        }
    }
    public void Limpiar(View v){
        Peso=findViewById(R.id.lblPeso);
        Altura=findViewById(R.id.lblAltura);
        Imc=findViewById(R.id.lblIMC);
        Peso.setText("");
        Altura.setText("");
        Imc.setText("");
    }
    public void Cerrar(View v){
        this.finish();
    }
    public boolean Vali(String peso, String altura){

        if (peso.toString().matches("") || altura.toString().matches("")){
            Toast.makeText(this, "Algun campo esta vacio.",Toast.LENGTH_SHORT).show();
        } else if (contieneLetras(peso) || contieneLetras(altura) || ValCa(peso) || ValCa(altura)) {
            Toast.makeText(this, "No introduzcas caracteres especiales o letras en los campos",Toast.LENGTH_SHORT).show();
        }
        else{
            return true;
        }
        return false;
    }
    public static boolean contieneLetras(String cadena) {
        return cadena.matches(".*[a-zA-Z].*");
    }

    public static boolean ValCa(String cadena) {
        return cadena.matches(".*[^a-zA-Z0-9].*");
    }
    public int Conver(String aop){
        return Integer.parseInt(aop);
    }
    public void inser(int peso, int altura){
        Imc=findViewById(R.id.lblIMC);
        DecimalFormat df = new DecimalFormat("#.00");
        double form = peso / ((altura / 100.0) * (altura / 100.0));
        String resultado = String.format("%.2f", form);
        Imc.setText(resultado + " kg/m²");
    }

}
